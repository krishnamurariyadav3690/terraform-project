terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.41.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
  shared_config_files      = ["/root/.aws/config"]
  shared_credentials_files = ["/root/.aws/credentials"]
}

terraform {
  backend "s3" {
    bucket = "production-terrform-bucket"
    key    = "terrform.tfstate"
    region = "ap-south-1"
  }
}

resource "aws_instance" "my_instacne_4" {
    ami = var.ami
    instance_type =  var.instance_type
    key_name = var.key_name
    tags = var.tags
}


